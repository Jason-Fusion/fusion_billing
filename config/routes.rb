# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

  get 'projects/:project_id/fusion_billing(/:year(/:month))' => 'fusion_billing#index', as: 'fusion_billing_date'
  get 'projects/:project_id/fusion_invoicing(/:year/:month)(/:locale)' => 'fusion_invoicing#index', as: 'fusion_invoicing_date'

  get '/fusion_reports' => 'fusion_reports#index'

  get '/fusion_reports/revenue(/:year(/:month))' => 'fusion_reports#revenue', as: 'revenue_report'

  get '/fusion_reports/invoicing(/:year(/:month))' => 'fusion_reports#invoicing', as: 'fusion_invoicing_report'
  post '/fusion_reports/invoicing/:year/:month/new' => 'fusion_reports#create_invoices', as: 'fusion_invoicing_create_invoices'

  resources :projects do
    member do
      get 'fusion_billing'
      get 'fusion_invoicing'
    end
  end
