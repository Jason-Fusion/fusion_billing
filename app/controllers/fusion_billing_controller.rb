class FusionBillingController < ApplicationController
  unloadable
  before_filter :find_project, :authorize, :only => :index

  def index
    @billing = FusionBilling.new(params[:project_id], params[:locale], params[:year], params[:month])
    respond_to do |format|
      format.html
      format.csv { send_data(@billing.current_entries.to_csv, filename: "billing_#{@billing.project.name.parameterize.underscore}.csv") }
    end
  end

  def find_project
    @project = Project.find(params[:project_id])
  end

end
