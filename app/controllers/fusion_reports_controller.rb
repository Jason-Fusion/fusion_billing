class FusionReportsController < ApplicationController
  before_filter :require_admin

  def index
  end

  def revenue
    @report = FusionRevenue.new(params)

    respond_to do |format|
      format.html
      format.csv { send_data(@report.to_csv, filename: "revenue.csv") }
    end

  end

  def invoicing
    @report = FusionInvoicing.new(params[:year], params[:month])
  end

  def create_invoices
    @report = FusionInvoicing.new(params[:year], params[:month])
    @standing_issues = @report.standing_issues
    !params[:projects].nil? && params[:projects].each do |issue_name, project|
      begin
      project = eval(project)
      issue = Issue.create!(
        project_id: project[:project_id],
        subject: "#{issue_name}#{@report.dates.begin.strftime(" [%Y/%m]")}",
        start_date: @report.dates.begin,
        tracker_id: Tracker.where(id: 62).first,
        author: User.find(222),
        priority: IssuePriority.where(name: 'Normal').first,
        description: "This issue was created by the Fusion Billing plugin.\n\"Billing\":#{url_for(controller: 'fusion_billing', action: 'index', project_id: Project.where(name: issue_name, status: 1).first.identifier, year: @report.dates.begin.year, month: @report.dates.begin.month)}\n\"Invoicing\":#{url_for(controller: 'fusion_invoicing', action: 'index', project_id: Project.where(name: issue_name, status: 1).first.identifier, year: @report.dates.begin.year, month: @report.dates.begin.month)}",
        custom_values: [CustomValue.new(custom_field_id: 86, value: project[:financial_account_manager])]
      ) if Issue.where(subject: issue_name + @report.dates.begin.strftime(" [%Y/%m]")).empty?
      rescue
        raise "Error with #{issue_name}"
      end
    end
    redirect_to(fusion_invoicing_report_path(year: @report.dates.begin.year, month: @report.dates.begin.month), notice: "Issues created")
  end

end
