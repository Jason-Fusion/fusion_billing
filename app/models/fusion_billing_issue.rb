class FusionBillingIssue < Issue
  attr_reader :format_issue

  STANDARD_ACTIVITIES     = ["Weekday: 08:00-20:00", "Weekday: 08:00-18:00", "Weekday: 09:00-18:00"]

  attr_accessor :value_to_fusion_once_off,
    :sow_type,
    :fixed_contractual_hourly_rate,
    :contractual_hours,
    :additional_service_fees,
    :weekday_08_00_20_00,
    :weekday_20_00_22_00,
    :weekday_22_00_05_00,
    :weekday_05_00_09_00,
    :weekday_05_00_08_00,
    :weekday_08_00_18_00,
    :weekday_18_00_09_00,
    :weekday_09_00_18_00,
    :weekday_18_00_22_00,
    :weekday_22_00_08_00,
    :weekend_holiday_anytime,
    :weekend_holiday_05_00_22_00,
    :weekend_holiday_22_00_05_00,
    :weekday_18_00_08_00,
    :weekday_20_00_08_00,
    :standard_regular_rate,
    :non_standard_junior_rate,
    :non_standard_regular_rate,
    :weekend_junior_rate,
    :weekend_regular_rate,
    :fsg_billable_rate,
    :weekday_half_day,
    :day_rate,
    :standard_junior_rate,
    :ignore_ro,
    :this_month_rollover_hours,
    :last_month_rollover_hours,
    :invoice_period,
    :has_call_out_fee,
    :this_month,
    :last_month,
    :synthetic,
    :standard_rate,
    :location,
    :tax_rate,
    :locale


  CUSTOM_FIELDS = [89, 91, 117, 136, 118, 131, 124, 126, 127, 138, 128, 132, 115, 114, 133,
     134, 113, 129, 130, 144, 179, 143, 145, 146, 147, 148, 166, 182, 184, 188, 227, 229,
     238, 239, 240, 140]

  scope :finance_issue, ->(project) {
      includes(:custom_values).where(
        subject: project.name, 
        project_id: 
          Project.
            where(
              "name like ?", "Finance%"
            ).map{ |project| project.id }, 
            tracker_id: [49, 53]
        )
  }

  def format_issue locale, last_month, this_month
    self.custom_values.inject({}){ |hash, cv|
      if CUSTOM_FIELDS.include?(cv.custom_field.id)
        self.send("#{cv.custom_field.name.parameterize.underscore}=", cv.value)
        @standard_rate = cv.value if STANDARD_ACTIVITIES.include?(cv.custom_field.name) && cv.value.present?
      end
    }
    @tax_rate = 0.08
    raise "Due date not found" if @sow_type == "fixed" && @due_date.nil?
    case @location.downcase
    when "la"
      @locale = locale || :en
    when "sh"
      @locale = locale || "zh-CN" 
    when "hk"
      @locale = locale || "zh-HK"
    when "tk"
      @locale = locale || :ja
    else
      @locale = :ja
    end
    self
  end

end
