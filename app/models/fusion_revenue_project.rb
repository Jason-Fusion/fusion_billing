class FusionRevenueProject < Project
  unloadable

  attr_accessor :finance_issue, :mapped_entries

  def get_custom_values
    @finance_issue.custom_values.each { |cv|
      @finance_issue.instance_variable_set("@#{cv.custom_field.name.parameterize.underscore}", cv.value)
    }
  end

  def beginning_date
    case @finance_issue.instance_variable_get("@sow_type")
    when "T&M"
      @dates.begin
    when "Fixed"
      @finance_issue.start_date
    when "Client Site"
      @finance_issue.start_date
    when "Maintenance"
      @dates.begin
    else
      "Error with date"
    end
  end

  def end_date
    case @finance_issue.instance_variable_get("@sow_type")
    when "T&M"
      @dates.end
    when "Fixed"
      @finance_issue.due_date
    when "Client Site"
      @finance_issue.due_date
    when "Maintenance"
      @dates.end
    else
      "Error with date"
    end
  end

  def total_charge
    case @finance_issue.instance_variable_get("@sow_type")
    when "T&M"
      self.time_entries.includes(:activity).where(spent_on: @dates).each.inject(0){ |sum, entry|
        sum + (entry.hours * @finance_issue.instance_variable_get("@#{entry.activity.name.parameterize.underscore}").to_f)
      }
    when "Fixed"
      @finance_issue.instance_variable_get("@value_to_fusion_once_off")
    when "Client Site"
      @finance_issue.instance_variable_get("@value_to_fusion_once_off")
    when "Maintenance"
      @finance_issue.instance_variable_get("@additional_service_fees")
    else
      "Error with SOW type"
    end
  end

end
