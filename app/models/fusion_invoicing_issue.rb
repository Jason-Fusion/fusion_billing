class FusionInvoicingIssue < Issue

  scope :standing_financial_issues, ->(dates) {
    includes(:tracker).where(tracker: [62], start_date: dates).order(:due_date)
  }

  scope :all_financial_issues, ->(dates) {
    includes(:project, :tracker).where(tracker: [49,53], status: 34).order(:tracker_id, :start_date, :due_date, :subject)
  }

end
