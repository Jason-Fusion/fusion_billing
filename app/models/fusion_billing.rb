class FusionBilling
  attr_reader :errors, :project, :sub_projects, :finance_issue, :current_entries, :previous_entries

  attr_accessor :last_month, :this_month

  def initialize project_id, locale = nil, year = nil, month = nil
    @errors = []

    @project = FusionBillingProject.this_project(project_id)
    @sub_projects = FusionBillingProject.sub_projects(@project)

    return unless @sub_projects.empty?

    self.send("this_month=", create_date_range(year, month))
    self.send("last_month=", create_date_range(year, month, :<<, 1))
 
    @finance_issue = FusionBillingIssue.finance_issue(@project).first
    if @finance_issue.nil?
      raise "No finance issue found"
    end
    @finance_issue = @finance_issue.format_issue(locale, @last_month, @this_month)

    @previous_entries = FusionBillingTimeEntry.new(@project, @last_month, @finance_issue)
    @current_entries = FusionBillingTimeEntry.new(@project, @this_month, @finance_issue, @previous_entries)

  rescue => error

    puts "====='error'=====\n#{error}\n==========\n"
    @errors.push(error)

  end

  def create_date_range year, month, operator = nil, amount = 0
    year  = year  ? year.to_i   : (Date.today.beginning_of_month << 1).year
    month = month ? month.to_i  : (Date.today.beginning_of_month << 1).month
    start_date  = Date.new(year, month, 1)
    start_date  = start_date.send(operator, amount) if amount > 0
    end_date    = (start_date >> 1) - 1

    (start_date..end_date)
  end

end
