class FusionBillingTimeEntry < TimeEntry
  attr_accessor :entries, :standard_hours, :non_standard_hours, :billable_hours,
    :non_billable_hours, :rollover_hours, :finance_issue, :billable_entries, :non_billable_entries,
    :total_charge, :invoice_number

  STANDARD_ACTIVITIES     = ["Weekday: 08:00-20:00", "Weekday: 08:00-18:00", "Weekday: 09:00-18:00", "Billable", "Fixed"]
  NON_BILLABLE_ACTIVITIES = ["Non billable", "Waived", "Travel"]

  def initialize project, month, finance_issue, previous_entries = nil
    @finance_issue = finance_issue
    @previous_entries = previous_entries
    @entries = {}

    invoice_issue = Issue.where(subject: "#{project.name} #{month.begin.strftime("[%Y/%m]")}").first 

    @invoice_number = invoice_issue.custom_values.where(custom_field_id: 230).first.value if invoice_issue.present?

    TimeEntry.includes(
        {issue: [:tracker]},
        :activity,
        :user).
      where(
        time_entries: {
          spent_on: month,
          project_id: project.id }
      ).each do |time_entry|
      @entries.merge!({ 
        time_entry.id => {
          :billable => (NON_BILLABLE_ACTIVITIES.include?(time_entry.activity.name) ? 0 : 1),
          :standard => (STANDARD_ACTIVITIES.include?(time_entry.activity.name) ? 1 : 0),
          :date => time_entry.spent_on,
          :in_charge => time_entry.user.name,
          :tracker => time_entry.issue.tracker.name,
          :activity => time_entry.activity.name,
          :comments => time_entry.comments,
          :hours => time_entry.hours,
          :rate => @finance_issue.instance_variable_get("@#{time_entry.activity.name.parameterize.underscore}"),
          :charge => @finance_issue.instance_variable_get("@#{time_entry.activity.name.parameterize.underscore}").nil? ?
            "" :
            @finance_issue.instance_variable_get("@#{time_entry.activity.name.parameterize.underscore}").to_f * time_entry.hours
        }})
    end

  end

  def billable_entries
    @entries.select{ |id, hash| hash[:billable] == 1 }
  end
  
  def non_billable_entries
    @entries.select{ |id, hash| hash[:billable] == 0 }
  end

  def standard_hours
    @entries.inject(0){ |sum, array| sum += array[1][:hours].to_f if array[1][:standard] == 1; sum }
  end

  def non_standard_hours
    @entries.inject(0){ |sum, array| sum += array[1][:hours].to_f if array[1][:standard] == 0 && array[1][:billable] == 1; sum }
  end

  def standard_hours_line_item
    if @finance_issue.contractual_hours.to_f == 0
      grouped = @entries.group_by{ |key, value| value[:activity] }
      selected = grouped.select{ |key, value| STANDARD_ACTIVITIES.include?(key) }
      mapped = selected.map{ |key, value| [key, value.inject(0){ |sum, array| sum += array[1][:hours].to_f; sum }, value[0][1][:rate], value.inject(0){ |sum, array| sum += array[1][:charge].to_f; sum }.round ] }
    elsif (standard_hours - @previous_entries.rollover_hours) > @finance_issue.contractual_hours.to_f
      [[
        "Over Contract Hours",
        standard_hours - @finance_issue.contractual_hours.to_f,
        @finance_issue.standard_rate,
        over_contractual_charge - contractual_charge
      ],[
        "Contract Hours",
        @finance_issue.contractual_hours.to_f,
        @finance_issue.fixed_contractual_hourly_rate,
        contractual_charge
      ]]
    else
      [[
        "Contract Hours",
        @finance_issue.contractual_hours,
        @finance_issue.fixed_contractual_hourly_rate,
        @finance_issue.fixed_contractual_hourly_rate.to_f * @finance_issue.contractual_hours.to_f
      ]]
    end
  end
  
  def non_standard_hours_line_item
    grouped = @entries.group_by{ |key, value| value[:activity] }
    selected = grouped.select{ |key, value| !STANDARD_ACTIVITIES.include?(key) && !NON_BILLABLE_ACTIVITIES.include?(key) }
    mapped = selected.map{ |key, value| [key, value.inject(0){ |sum, array| sum += array[1][:hours].to_f; sum }, value[0][1][:rate], value.inject(0){ |sum, array| sum += array[1][:charge].to_f; sum}.round ] }
  end

  def additional_services_line_item
    @finance_issue.additional_service_fees.to_f > 0 ? [["Monthly Service Fee", "-", "-", @finance_issue.additional_service_fees]] : []
  end

  def rolled_over_hours
    @finance_issue.ignore_ro == "0" ?
      @previous_entries.rollover_hours : 0
  end

  def billable_hours
    billable_hours = @entries.inject(0){ |sum, array| sum += array[1][:hours].to_f if array[1][:billable] == 1; sum }
    billable_hours - @previous_entries.rollover_hours if @finance_issue.ignore_ro == "0"
    billable_hours
  end

  def non_billable_hours
    @entries.inject(0){ |sum, array| sum += array[1][:hours].to_f if array[1][:billable] == 0; sum }
  end

  def total_hours
    (standard_hours - rolled_over_hours) + non_standard_hours + non_billable_hours
  end

  def standard_charge
    @entries.inject(0){ |sum, array| sum += array[1][:charge].to_f if array[1][:billable] == 1 && array[1][:standard] == 1; sum }
  end

  def non_standard_charge
    @entries.inject(0){ |sum, array| sum += array[1][:charge].to_f if array[1][:standard] == 0 && array[1][:billable] == 1; sum }
  end

  def billable_charge
    @entries.inject(0){ |sum, array| sum += array[1][:charge].to_f if array[1][:billable] == 1; sum }
  end

  def contractual_charge
    (@finance_issue.contractual_hours.to_f * @finance_issue.fixed_contractual_hourly_rate.to_f)
  end

  def over_contractual_charge
    (((standard_hours - @finance_issue.contractual_hours.to_f) * @finance_issue.standard_rate.to_f) + contractual_charge)
  end

  def fsg_charge
    (billable_hours * @finance_issue.fsg_billable_rate.to_f).round
  end

  def fixed_charge
    @finance_issue.value_to_fusion_once_off.to_f
  end

  def fixed_tax
    fixed_charge * @finance_issue.tax_rate
  end

  def fsg_tax
    fsg_charge * @finance_issue.tax_rate
  end

  def fsg_total_charge
    fsg_charge + fsg_tax
  end

  def service_charge
    @finance_issue.additional_service_fees.to_f
  end

  def service_tax
    service_charge * @finance_issue.tax_rate.to_f
  end

  def service_total_charge
    service_charge + service_tax
  end

  def total_charge
    total = 0.0
    total += (standard_hours - rolled_over_hours < @finance_issue.contractual_hours.to_f ?
                contractual_charge : over_contractual_charge)
    total += non_standard_charge
    total += @finance_issue.additional_service_fees.to_f
    total.round
  end

  def tax
    @finance_issue.location == "TK" ? (total_charge * @finance_issue.tax_rate).round : 0
  end

  def billable_ratio
    ( (standard_hours + non_standard_hours) / total_hours ) * 100
  end

  def non_billable_ratio
    ( non_billable_hours / total_hours ) * 100
  end

  def rollover_hours
    return 0 if @finance_issue.ignore_ro == "1"
    hours = @finance_issue.contractual_hours.to_f - (standard_hours)
    hours > 0 ? hours : 0.0
  end

  def to_csv
    csv_data = []

    csv_data.push(
      [
        "\"Date\"",
        "\"In Charge\"",
        "\"Tracker\"",
        "\"Activity\"",
        "\"Comments\"",
        "\"Hours\""
      ].join(',')
    )

    @entries.each do |entry|
      csv_data.push(
        [
          "\"#{entry[1][:date]}\"",
          "\"#{entry[1][:in_charge]}\"",
          "\"#{entry[1][:tracker]}\"",
          "\"#{entry[1][:activity]}\"",
          "\"#{entry[1][:comments]}\"",
          "\"#{entry[1][:hours]}\""
        ].join(',')
      )
    end

    csv_data.join("\n")
  end

  def empty?
    @entries.empty?
  end

end
