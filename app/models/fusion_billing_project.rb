class FusionBillingProject < Project

  scope :this_project, ->(project_name) { where(identifier: project_name).first }

  scope :sub_projects, ->(project) { where(parent_id: project.id, status: 1).order("LOWER(projects.name)") }

end
