class FusionRevenue
  attr_accessor :itms_projects, :finance_issue_subjects, :finance_issues

  ITMS_PROJECTS = {
    tokyo: 1,
    goldman_sachs: 944,
    hong_kong: 264,
    shanghai: 226,
    los_angeles: 107
  }

  FINANCE_PROJECTS = {
    tokyo: 813,
    hong_kong: 816,
    shanghai: 814,
    los_angeles: 815
  }

  FINANCE_TRACKERS = {
    support: 49,
    delivery: 53,
    #invoicing: 62
  }

  def initialize params
    @dates = create_date_range(params[:year], params[:month])
    @finance_issues = get_finance_issues
    @finance_issue_subjects = @finance_issues.map{ |issue| issue.subject.strip.downcase }
    @itms_projects = get_itms_projects.select!{ |project|
      if finance_issue_subjects.include?(project.name.strip.downcase)
        project.
          instance_variable_set(
              :@finance_issue,
              @finance_issues.select{ |issue|
                issue.subject.strip.downcase == project.name.strip.downcase
              }.first
            )
        project.instance_variable_set("@dates", @dates)
        project.get_custom_values
      end

      (
        finance_issue_subjects.include?(project.name.strip.downcase) &&
        !["Manual", "Client Site"].include?(project.finance_issue.instance_variable_get("@sow_type"))
      )
    }
  end

  def get_itms_projects
    FusionRevenueProject.
      where(
        id:       [ITMS_PROJECTS[:tokyo], ITMS_PROJECTS[:goldman_sachs]],
        status:   1
        ).
      map { |project|
        FusionRevenueProject.
          #includes(
          #  time_entries: [:activity]
          #  ).
          where(
            FusionRevenueProject.
              find(
                project.id
              ).
              project_condition(
                true
              )
            ).
          where(
            status: 1
            ).
          #where(
          #  time_entries: {
          #    spent_on: (Date.new(2015,4,1)..Date.new(2015,4,-1))
          #    }
          #  ).
          order(:name)
        }.
      flatten
  end

  def get_finance_issues

    FusionRevenueIssue.
      includes(
        :custom_values
        ).
      where(
        #subject: FusionRevenueProject.
        #  where(
        #    id: ITMS_PROJECTS[:tokyo]
        #    ).
        #  sorted.
        #  map{ |project|
        #    FusionRevenueProject.
        #      where(
        #        FusionRevenueProject.
        #          find(
        #            project.id
        #            ).
        #          project_condition(
        #            true
        #            )
        #        ).
        #      sorted.
        #      map{ |project|
        #        project.name.strip
        #        }
        #    }.
        #  flatten,
        tracker: FINANCE_TRACKERS.values,
        status_id: 34
        ).
      where(
        "IFNULL(start_date,?) <= ? AND IFNULL(due_date,?) >= ?", @dates.end, @dates.end, @dates.begin, @dates.begin
        )

  end

  def to_csv

    csv_data = []

    csv_data.push(
      [
        "\"Project\"",
        "\"SOW Type\"",
        "\"Beginning Date\"",
        "\"End Date\"",
        "\"Revenue\""
      ].join(",")
    )

    @itms_projects.each do |itms_project|
      csv_data.push(
        [
          "\"#{itms_project.name}\"",
          "\"#{itms_project.finance_issue.instance_variable_get("@sow_type")}\"",
          "\"#{itms_project.beginning_date}\"",
          "\"#{itms_project.end_date}\"",
          "\"#{itms_project.total_charge}\"",
        ].join(",")
      )
    end

    csv_data.join("\n")

  end

  def create_date_range year, month, operator = nil, amount = 0
    year  = year  ? year.to_i   : (Date.today.beginning_of_month << 1).year
    month = month ? month.to_i  : (Date.today.beginning_of_month << 1).month
    start_date  = Date.new(year, month, 1)
    start_date  = start_date.send(operator, amount) if amount > 0
    end_date    = (start_date >> 1) - 1

    (start_date..end_date)
  end

end
