class FusionInvoicing
  attr_accessor :financial_projects, :dates, :all_issues, :standing_issues, :non_invoiced_issues
  
  def initialize year, month
    @dates = create_date_range(year, month)

    @financial_projects = FusionInvoicingProject.invoicing

    @standing_issues = FusionInvoicingIssue.standing_financial_issues(@dates)
    @all_issues = FusionInvoicingIssue.all_financial_issues(@dates)

    @non_invoiced_issues = @all_issues.map{ |object|
      invoice = @standing_issues.where(subject: (object.subject + @dates.begin.strftime(" [%Y/%m]"))).first
      name = object.custom_values.where(custom_field_id: 86).first.value
      {
        project_id: object.project.id,
        financial_tracker: object.tracker.name,
        financial_subject: object.subject,
        financial_id: object.id,
        financial_start_date: object.start_date,
        financial_due_date: object.due_date,
        financial_account_manager: name,
        financial_generate_next_invoice_after: ( Date.parse(object.custom_values.where(custom_field_id: 149).first.value) rescue @dates.begin.beginning_of_month ),
        invoice_tracker: (invoice ? invoice.tracker.name : nil),
        invoice_subject: (invoice ? invoice.subject : nil),
        invoice_id: (invoice ? invoice.id : nil),
      }
    }

    self
  end

  def create_date_range year, month, operator = nil, amount = 0
    year  = year  ? year.to_i   : Date.today.year
    month = month ? month.to_i  : Date.today.month
    start_date  = Date.new(year, month, 1)
    start_date  = start_date.send(operator, amount) if amount > 0
    end_date    = (start_date >> 1) - 1

    (start_date..end_date)
  end
end
