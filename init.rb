Redmine::Plugin.register :fusion_billing do
  name 'Fusion Billing plugin'
  author 'Jason Zudell'
  description 'A plugin to show billing information.'
  version '0.0.1'
  url 'https://gitlab.com/Jason-Fusion/fusion_billing'

  permission :fusion_invoicing,
    { fusion_invoicing: [:index] },
    require: :member
  menu :project_menu,
    :fusion_invoicing,
    { controller: 'fusion_invoicing', action: 'index' },
    caption: 'Fusion Invoicing',
    after: :files,
    param: :project_id
 
  permission :fusion_billing,
    { fusion_billing: [:index] },
    require: :member
  menu :project_menu,
    :fusion_billing,
    { controller: 'fusion_billing', action: 'index' },
    caption: 'Fusion Billing',
    after: :files,
    param: :project_id

  permission :fusion_revenue_report,
    { fusion_reports: [:revenue] },
    require: :member
  permission :fusion_invoice_report,
    { fusion_reports: [:invoicing] },
    require: :member
  menu :top_menu,
    :fusion_reports,
    { controller: 'fusion_reports', action: 'index' },
    caption: 'Reports',
    before: :administration

end
