$(document).ready(function() {
  $('#check_all').click(function() {
    if(this.checked) {
      $('.create-invoice-checkbox').each(function() {
        this.checked = true;
      });
    } else {
      $('.create-invoice-checkbox').each(function() {
        this.checked = false;
      });

    }
  });
});
